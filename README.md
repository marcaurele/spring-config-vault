# Spring Config with Vault

[![pipeline status](https://gitlab.com/marcaurele/spring-config-vault/badges/master/pipeline.svg)](https://gitlab.com/marcaurele/spring-config-vault/commits/master)


A sample application to show case how to set up a Java application
with Spring Cloud Server for the configuration and Hashicorp Vault
for sensitive data management.

The configuration project can be found at
https://gitlab.com/marcaurele/spring-config-vault-a-bootiful-client


## URLs

    # To fetch the message
    curl -s http://localhost:8080/message | jq
    
    # To fetch the secret
    curl -s http://localhost:8080/secret | jq
    
    # To refresh the configuration insde the app
    curl -s --request POST http://localhost:8080/actuator/refresh | jq

    # To fetch the configuration as the application
    curl -s http://localhost:8888/a-bootiful-client/ad | jq

    # to fetch some information about the application
    curl -s http://localhost:8080/actuator/info
    curl -s http://localhost:8080/actuator/health

## Config server REST endpoints

Those are the endpoints available from the config server:

- `/{application}/{profile}[/{label}]`
- `/{application}-{profile}.yml`
- `/{label}/{application}-{profile}.yml`
- `/{application}-{profile}.properties`
- `/{label}/{application}-{profile}.properties`

So for example, if your application name is `crazyapp`, you can get
the configuration values for the `testing` profile on those endpoints:

- `/crazyapp/testing`
- `/crazyapp-testing.yml`
- `/crazyapp-testing.properties`

## Docker

### Embedded version

To build the *embedded* version of the application:

    docker build --file=Dockerfile.embedded -t marcaurele/spring-config-vault-embedded --rm .

To run it atferwards:

    docker run -p "8080:8081" marcaurele/spring-config-vault-embedded

### Client-Server

To run and build the client / server version in docker containers:

    docker-compose build

To run it (and rebuild it in case):

    docker-compose up --build

### Vault

To add a secret in the KV store:

    docker exec -e VAULT_TOKEN=devtoken -e VAULT_ADDR=http://localhost:8200 vault vault kv put secret/password something=secret
