package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableConfigServer
public class ConfigClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication.class, args);
    }
}

@RefreshScope
@RestController
class MessageRestController {

    @Value("${message:Hello from Java class}")
    private String message;

    @Value("${secret:Not secret}")
    private String secret;

    @Value("${global.variable}")
    private String globalConf;

    @Autowired
    Environment env;

    @RequestMapping("/message")
    @ResponseBody
    MessageOutput getMessage() {
        return new MessageOutput(env.getActiveProfiles(), this.message);
    }

    @RequestMapping("/secret")
    @ResponseBody
    MessageOutput getSecret() {
        return new MessageOutput(env.getActiveProfiles(), this.secret);
    }

    @RequestMapping("/global")
    @ResponseBody
    MessageOutput getGlobal() {
        return new MessageOutput(env.getActiveProfiles(), this.globalConf);
    }

}

class MessageOutput {
    private String[] profiles;
    private String messsage;

    public MessageOutput(String[] profiles, String messsage) {
        this.profiles = profiles;
        this.messsage = messsage;
    }

    public String[] getProfiles() {
        return profiles;
    }

    public String getMesssage() {
        return messsage;
    }
}